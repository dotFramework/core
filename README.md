[![Build status](https://gitlab.com/dotFramework/core/badges/master/pipeline.svg)](https://ci.appveyor.com/project/dotFramework/core)
[![NuGet Release](https://img.shields.io/nuget/vpre/DotFramework.Core.svg)](https://www.nuget.org/packages/DotFramework.Core)
[![NuGet Downloads](https://img.shields.io/nuget/dt/DotFramework.Core.svg)](https://www.nuget.org/packages/DotFramework.Core)
[![License](https://img.shields.io/badge/license-apache%202.0-60C060.svg)](https://github.com/dotFramework/core/blob/master/LICENSE)

# dotFramework Core