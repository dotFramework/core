﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace DotFramework.Core.Web
{
    public abstract partial class AbstractHttpUtility : IDisposable
    {
        #region Constructor

        public AbstractHttpUtility()
        {
            Accept = "application/json";
            ContentType = "application/json";
        }

        public AbstractHttpUtility(string baseAddress) : this()
        {
            BaseAddress = new Uri(baseAddress);
        }

        public AbstractHttpUtility(string baseAddress, IDictionary<String, String> defaultHeaders) : this(baseAddress)
        {
            DefaultHeaders = defaultHeaders;
        }

        #endregion

        #region Properties

        public Uri BaseAddress { get; private set; }

        public IDictionary<String, String> DefaultHeaders { get; private set; }

        /// <summary>
        /// Gets or sets the time-out value in milliseconds
        /// </summary>
        public int Timeout { get; set; }

        public string Accept { get; set; }

        public string ContentType { get; set; }

        #endregion

        #region Sync

        #region Post

        public TResponseModel Post<TResponseModel>(string path)
        {
            return (TResponseModel)Post(typeof(TResponseModel), path);
        }

        public object Post(Type responseModelType, string path)
        {
            return Post<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path
            });
        }

        public TResponseModel Post<TResponseModel>(string path, object model)
        {
            return (TResponseModel)Post(typeof(TResponseModel), path, model);
        }

        public object Post(Type responseModelType, string path, object model)
        {
            return Post<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model
            });
        }

        public TResponseModel Post<TResponseModel>(string path, AuthorizationToken token)
        {
            return (TResponseModel)Post(typeof(TResponseModel), path, token);
        }

        public object Post(Type responseModelType, string path, AuthorizationToken token)
        {
            return Post<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                Token = token
            });
        }

        public TResponseModel Post<TResponseModel>(string path, object model, AuthorizationToken token)
        {
            return (TResponseModel)Post(typeof(TResponseModel), path, model, token);
        }

        public object Post(Type responseModelType, string path, object model, AuthorizationToken token)
        {
            return Post<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model,
                Token = token
            });
        }

        public TResponseModel Post<TResponseModel, TErrorResult>(string path) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Post<TErrorResult>(typeof(TResponseModel), path);
        }

        public object Post<TErrorResult>(Type responseModelType, string path) where TErrorResult : ErrorResult
        {
            return Post<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path
            });
        }

        public TResponseModel Post<TResponseModel, TErrorResult>(string path, object model) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Post<TErrorResult>(typeof(TResponseModel), path, model);
        }

        public object Post<TErrorResult>(Type responseModelType, string path, object model) where TErrorResult : ErrorResult
        {
            return Post<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model
            });
        }

        public TResponseModel Post<TResponseModel, TErrorResult>(string path, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Post<TErrorResult>(typeof(TResponseModel), path, token);
        }

        public object Post<TErrorResult>(Type responseModelType, string path, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return Post<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                Token = token
            });
        }

        public TResponseModel Post<TResponseModel, TErrorResult>(string path, object model, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Post<TErrorResult>(typeof(TResponseModel), path, model, token);
        }

        public object Post<TErrorResult>(Type responseModelType, string path, object model, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return Post<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model,
                Token = token
            });
        }

        public TResponseModel Post<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Post<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public TResponseModel Post<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)Post<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public object Post(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            return Post<DefaultErrorResult>(responseModelType, requestObject);
        }

        public abstract object Post<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Get

        public TResponseModel Get<TResponseModel>(string path)
        {
            return (TResponseModel)Get(typeof(TResponseModel), path);
        }

        public object Get(Type responseModelType, string path)
        {
            return Get<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path
            });
        }

        public TResponseModel Get<TResponseModel>(string path, IEnumerable<KeyValuePair<String, String>> parameters)
        {
            return (TResponseModel)Get(typeof(TResponseModel), path, parameters);
        }

        public object Get(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters)
        {
            return Get<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters
            });
        }

        public TResponseModel Get<TResponseModel>(string path, AuthorizationToken token)
        {
            return (TResponseModel)Get(typeof(TResponseModel), path, token);
        }

        public object Get(Type responseModelType, string path, AuthorizationToken token)
        {
            return Get<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Token = token
            });
        }

        public TResponseModel Get<TResponseModel>(string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token)
        {
            return (TResponseModel)Get(typeof(TResponseModel), path, parameters, token);
        }

        public object Get(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token)
        {
            return Get<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters,
                Token = token
            });
        }

        public TResponseModel Get<TResponseModel, TErrorResult>(string path) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Get<TErrorResult>(typeof(TResponseModel), path);
        }

        public object Get<TErrorResult>(Type responseModelType, string path) where TErrorResult : ErrorResult
        {
            return Get<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path
            });
        }

        public TResponseModel Get<TResponseModel, TErrorResult>(string path, IEnumerable<KeyValuePair<String, String>> parameters) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Get<TErrorResult>(typeof(TResponseModel), path, parameters);
        }

        public object Get<TErrorResult>(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters) where TErrorResult : ErrorResult
        {
            return Get<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters
            });
        }

        public TResponseModel Get<TResponseModel, TErrorResult>(string path, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Get<TErrorResult>(typeof(TResponseModel), path, token);
        }

        public object Get<TErrorResult>(Type responseModelType, string path, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return Get<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Token = token
            });
        }

        public TResponseModel Get<TResponseModel, TErrorResult>(string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Get<TErrorResult>(typeof(TResponseModel), path, parameters, token);
        }

        public object Get<TErrorResult>(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return Get<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters,
                Token = token
            });
        }

        public TResponseModel Get<TResponseModel, TErrorResult>(HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Get<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public TResponseModel Get<TResponseModel>(HttpRequestQueryObject requestObject)
        {
            return (TResponseModel)Get<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public object Get(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return Get<DefaultErrorResult>(responseModelType, requestObject);
        }

        public abstract object Get<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Delete

        public TResponseModel Delete<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)Delete<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public TResponseModel Delete<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Delete<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract object Delete<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        public TResponseModel Delete<TResponseModel>(HttpRequestQueryObject requestObject)
        {
            return (TResponseModel)Delete<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public TResponseModel Delete<TResponseModel, TErrorResult>(HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Delete<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract object Delete<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Put

        public TResponseModel Put<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)Put<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public TResponseModel Put<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)Put<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract object Put<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #endregion

        #region Async

        #region Post

        public async Task<TResponseModel> PostAsync<TResponseModel>(string path)
        {
            return (TResponseModel)await PostAsync(typeof(TResponseModel), path);
        }

        public Task<object> PostAsync(Type responseModelType, string path)
        {
            return PostAsync<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel>(string path, object model)
        {
            return (TResponseModel)await PostAsync(typeof(TResponseModel), path, model);
        }

        public Task<object> PostAsync(Type responseModelType, string path, object model)
        {
            return PostAsync<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel>(string path, AuthorizationToken token)
        {
            return (TResponseModel)await PostAsync(typeof(TResponseModel), path, token);
        }

        public Task<object> PostAsync(Type responseModelType, string path, AuthorizationToken token)
        {
            return PostAsync<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                Token = token
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel>(string path, object model, AuthorizationToken token)
        {
            return (TResponseModel)await PostAsync(typeof(TResponseModel), path, model, token);
        }

        public Task<object> PostAsync(Type responseModelType, string path, object model, AuthorizationToken token)
        {
            return PostAsync<DefaultErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model,
                Token = token
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel, TErrorResult>(string path) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await PostAsync<TErrorResult>(typeof(TResponseModel), path);
        }

        public Task<object> PostAsync<TErrorResult>(Type responseModelType, string path) where TErrorResult : ErrorResult
        {
            return PostAsync<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel, TErrorResult>(string path, object model) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await PostAsync<TErrorResult>(typeof(TResponseModel), path, model);
        }

        public Task<object> PostAsync<TErrorResult>(Type responseModelType, string path, object model) where TErrorResult : ErrorResult
        {
            return PostAsync<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel, TErrorResult>(string path, object model, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await PostAsync<TErrorResult>(typeof(TResponseModel), path, model, token);
        }

        public Task<object> PostAsync<TErrorResult>(Type responseModelType, string path, object model, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return PostAsync<TErrorResult>(responseModelType, new HttpRequestBodyObject
            {
                Path = path,
                PayLoad = model,
                Token = token
            });
        }

        public async Task<TResponseModel> PostAsync<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await PostAsync<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public async Task<TResponseModel> PostAsync<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)await PostAsync<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public Task<object> PostAsync(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            return PostAsync<DefaultErrorResult>(responseModelType, requestObject);
        }

        public abstract Task<object> PostAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Get

        public async Task<TResponseModel> GetAsync<TResponseModel>(string path)
        {
            return (TResponseModel)await GetAsync(typeof(TResponseModel), path);
        }

        public Task<object> GetAsync(Type responseModelType, string path)
        {
            return GetAsync<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel>(string path, IEnumerable<KeyValuePair<String, String>> parameters)
        {
            return (TResponseModel)await GetAsync(typeof(TResponseModel), path, parameters);
        }

        public Task<object> GetAsync(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters)
        {
            return GetAsync<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel>(string path, AuthorizationToken token)
        {
            return (TResponseModel)await GetAsync(typeof(TResponseModel), path, token);
        }

        public Task<object> GetAsync(Type responseModelType, string path, AuthorizationToken token)
        {
            return GetAsync<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Token = token
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel>(string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token)
        {
            return (TResponseModel)await GetAsync(typeof(TResponseModel), path, parameters, token);
        }

        public Task<object> GetAsync(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token)
        {
            return GetAsync<DefaultErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters,
                Token = token
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel, TErrorResult>(string path) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await GetAsync<TErrorResult>(typeof(TResponseModel), path);
        }

        public Task<object> GetAsync<TErrorResult>(Type responseModelType, string path) where TErrorResult : ErrorResult
        {
            return GetAsync<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel, TErrorResult>(string path, IEnumerable<KeyValuePair<String, String>> parameters) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await GetAsync<TErrorResult>(typeof(TResponseModel), path, parameters);
        }

        public Task<object> GetAsync<TErrorResult>(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters) where TErrorResult : ErrorResult
        {
            return GetAsync<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel, TErrorResult>(string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await GetAsync<TErrorResult>(typeof(TResponseModel), path, parameters, token);
        }

        public Task<object> GetAsync<TErrorResult>(Type responseModelType, string path, IEnumerable<KeyValuePair<String, String>> parameters, AuthorizationToken token) where TErrorResult : ErrorResult
        {
            return GetAsync<TErrorResult>(responseModelType, new HttpRequestQueryObject
            {
                Path = path,
                Parameters = parameters,
                Token = token
            });
        }

        public async Task<TResponseModel> GetAsync<TResponseModel, TErrorResult>(HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await GetAsync<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public async Task<TResponseModel> GetAsync<TResponseModel>(HttpRequestQueryObject requestObject)
        {
            return (TResponseModel)await GetAsync<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public Task<object> GetAsync(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return GetAsync<DefaultErrorResult>(responseModelType, requestObject);
        }

        public abstract Task<object> GetAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Delete

        public async Task<TResponseModel> DeleteAsync<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)await DeleteAsync<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public async Task<TResponseModel> DeleteAsync<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await DeleteAsync<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        public async Task<TResponseModel> DeleteAsync<TResponseModel>(HttpRequestQueryObject requestObject)
        {
            return (TResponseModel)await DeleteAsync<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public async Task<TResponseModel> DeleteAsync<TResponseModel, TErrorResult>(HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await DeleteAsync<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #region Put

        public async Task<TResponseModel> PutAsync<TResponseModel>(HttpRequestBodyObject requestObject)
        {
            return (TResponseModel)await PutAsync<DefaultErrorResult>(typeof(TResponseModel), requestObject);
        }

        public async Task<TResponseModel> PutAsync<TResponseModel, TErrorResult>(HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult
        {
            return (TResponseModel)await PutAsync<TErrorResult>(typeof(TResponseModel), requestObject);
        }

        public abstract Task<object> PutAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject) where TErrorResult : ErrorResult;

        #endregion

        #endregion

        #region Helpers

        protected string FixJson(string json)
        {
            //return json.Replace("\"[", "[").Replace("]\"", "]").Replace("\"{", "{").Replace("}\"", "}").Replace("\\", "").Replace("\\\"", "\"").Replace("\"[", "[").Replace("]\"", "]").Replace("\"{", "{").Replace("}\"", "}");
            return json;
        }

        protected string CreateQueryString(IEnumerable<KeyValuePair<String, String>> parameters)
        {
            try
            {
                if (parameters != null && parameters.Count() != 0)
                {
                    return String.Join("&", parameters.Select(kvp => string.Format("{0}={1}", kvp.Key, kvp.Value)));
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected string AppendQueryString(string path, IEnumerable<KeyValuePair<String, String>> parameters)
        {
            string queryString = CreateQueryString(parameters);

            if (!String.IsNullOrEmpty(queryString))
            {
                if (path.Contains('?'))
                {
                    return String.Format("{0}&{1}", path, queryString);
                }
                else
                {
                    return String.Format("{0}?{1}", path, queryString);
                }
            }
            else
            {
                return path;
            }
        }

        protected Exception HandleException<TErrorResult>(HttpStatusCode statusCode, string content) where TErrorResult : ErrorResult
        {
            if (statusCode == HttpStatusCode.Unauthorized)
            {
                return new UnauthorizedHttpException();
            }
            if (statusCode == HttpStatusCode.Forbidden)
            {
                return new ForbiddenAccessException();
            }
            else if (statusCode == HttpStatusCode.BadRequest || statusCode == HttpStatusCode.InternalServerError)
            {
                return CreateAPIException<TErrorResult>(content);
            }
            else
            {
                return new HttpException("Unable to perform Http action.", content);
            }
        }

        private static Exception CreateAPIException<TErrorResult>(string content) where TErrorResult : ErrorResult
        {
            if (typeof(TErrorResult) == typeof(DefaultErrorResult))
            {
                JObject obj;
                try
                {
                    obj = JObject.Parse(content);
                }
                catch
                {
                    return new HttpException("Unable to perform Http action.", content);
                }

                if (obj.ContainsKey("ModelState"))
                {
                    return new HttpException<ModelStateErrorResult>("Unable to perform Http action.", obj);
                }
                else if (obj.ContainsKey("Message"))
                {
                    return new HttpException<BadRequestErrorResult>("Unable to perform Http action.", obj);
                }
                else if (obj.ContainsKey("ErrorCode") && obj.ContainsKey("Extensions"))
                {
                    var errorResult = obj.ToObject<ProblemDetailsErrorResult>();

                    if (errorResult.Type.Contains("custom-exception"))
                    {
                        return new ProblemDetailsCustomHttpException(errorResult.Title, obj);
                    }
                    else
                    {
                        return new ProblemDetailsHttpException("Unable to perform Http action.", obj);
                    }
                }
                else
                {
                    return new HttpException("Unable to perform Http action.", content);
                }
            }
            else
            {
                try
                {
                    if (typeof(IUnauthorizedErrorResult).IsAssignableFrom(typeof(TErrorResult)))
                    {
                        return new UnauthorizedHttpException<TErrorResult>(content);
                    }
                    else
                    {
                        return new HttpException<TErrorResult>("Unable to perform Http action.", content);
                    }
                }
                catch (Exception)
                {
                    return new HttpException(content);
                }
            }
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {

        }

        #endregion
    }
}
