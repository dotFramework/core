﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

using System.IO;
using System.Linq;


#if NETCOREAPP3_1
using Microsoft.Extensions.DependencyInjection;
#endif

namespace DotFramework.Core.Web
{
    public abstract class AbstractHttpClientUtility : AbstractHttpUtility
    {
        #region Variables

        protected HttpClient _HttpClient;


#if NETCOREAPP3_1
        protected readonly string _HttpClientName;
        protected IHttpClientFactory _HttpClientFactory;
        protected bool _SingleInstance;
#endif

        private static object _lock = new object();

        #endregion

        #region Constructor

#if NETCOREAPP3_1

        protected AbstractHttpClientUtility(string httpClientName) : this(httpClientName, false)
        {

        }

        protected AbstractHttpClientUtility(string httpClientName, bool singleInstance) : base()
        {
            _HttpClientName = httpClientName;
            _SingleInstance = singleInstance;
        }

#endif

        public AbstractHttpClientUtility(string baseAddress, IDictionary<String, String> defaultHeaders) : base(baseAddress, defaultHeaders)
        {
#if NETCOREAPP3_1
            _HttpClientFactory = ServiceContext.Instance.ServiceProvider?.GetService<IHttpClientFactory>();
#endif
        }

        #endregion

        #region Abstract Methods

        protected abstract HttpClient CreateHttpClient();

        #endregion

        #region Sync

        #region Post

        public override object Post<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Post, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get

        public override object Get<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Get, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Delete

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Delete, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Delete, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Put

        public override object Put<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Put, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion

        #region Async

        #region Post

        public override async Task<object> PostAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Post, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get

        public override async Task<object> GetAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await SendAsync<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Get, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Delete

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Delete, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await SendAsync<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Delete, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Put

        public override async Task<object> PutAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Put, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion

        #region Helpers

        private HttpContent GetContent(object model)
        {
            HttpContent content;

            if (model is HttpContent)
            {
                content = model as HttpContent;
            }
            else if (model is IEnumerable<KeyValuePair<String, String>>)
            {
                content = GetUrlEncodedRequestContent(model as IEnumerable<KeyValuePair<String, String>>);
            }
            else
            {
                content = GetStringContent(model);
            }

            return content;
        }

        private StringContent GetStringContent(object model)
        {
            try
            {
                StringContent content = null;

                if (model != null)
                {
                    content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, ContentType);
                }

                return content;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private FormUrlEncodedContent GetUrlEncodedRequestContent(IEnumerable<KeyValuePair<String, String>> parameters)
        {
            try
            {
                return new FormUrlEncodedContent(parameters);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private object Send<TErrorResult>(Type responseModelType, string path, HttpMethod method, HttpContent content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            HttpRequestMessage request = CreateRequest(path, method, content, token, headers);

            var client = GetHttpClientInternal();
            var result = client.SendAsync(request).Result;

            String json;

            if (result.Content.Headers.ContentEncoding.ToReadOnlyCollection().Contains("gzip", StringComparer.InvariantCultureIgnoreCase))
            {
                json = GetCompressedResult(result.Content.ReadAsByteArrayAsync().Result);
            }
            else
            {
                json = result.Content.ReadAsStringAsync().Result;
            }

            json = FixJson(json);

            if (result.IsSuccessStatusCode)
            {
                if (responseModelType == typeof(String))
                {
                    return json;
                }
                else
                {
                    try
                    {
                        return JsonConvert.DeserializeObject(json, responseModelType);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Invalid JSON response: {json}", ex);
                    }
                }
            }
            else
            {
                throw HandleException<TErrorResult>(result.StatusCode, json);
            }
        }

        private async Task<object> SendAsync<TErrorResult>(Type responseModelType, string path, HttpMethod method, HttpContent content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            HttpRequestMessage request = CreateRequest(path, method, content, token, headers);

            var client = GetHttpClientInternal();

            if (client == null)
            {
                throw new NullReferenceException("Client is not ready");
            }

            HttpResponseMessage result;

            result = await client.SendAsync(request);

            if (result?.Content == null)
            {
                throw new NullReferenceException("Result is not ready");
            }

            String json;

            if (result.Content.Headers.ContentEncoding.ToReadOnlyCollection().Contains("gzip", StringComparer.InvariantCultureIgnoreCase))
            {
                json = GetCompressedResult(await result.Content.ReadAsByteArrayAsync());
            }
            else
            {
                json = result.Content.ReadAsStringAsync().Result;
            }

            if (result.IsSuccessStatusCode)
            {
                if (responseModelType == typeof(String))
                {
                    return json;
                }
                else
                {
                    return JsonConvert.DeserializeObject(json, responseModelType);
                }
            }
            else
            {
                throw HandleException<TErrorResult>(result.StatusCode, json);
            }
        }

        private string GetCompressedResult(byte[] bytes)
        {
            using (var msi = new MemoryStream(bytes))
            using (var mso = new MemoryStream())
            {
                using (var gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    gs.CopyTo(mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }

        private HttpRequestMessage CreateRequest(string path, HttpMethod method, HttpContent content, AuthorizationToken token, IDictionary<String, String> headers)
        {
            HttpRequestMessage request = new HttpRequestMessage(method, path);

            if (content != null)
            {
                request.Content = content;
            }

            if (token != null)
            {
                request.Headers.Authorization = new AuthenticationHeaderValue(token.TokenType.ToString(), token.Token);
            }

            if (headers != null)
            {
                foreach (var headerItem in headers)
                {
                    request.Headers.Add(headerItem.Key, headerItem.Value);
                }
            }

            return request;
        }

        private HttpClient GetHttpClientInternal()
        {
#if NETCOREAPP3_1
            _HttpClientFactory = ServiceContext.Instance.ServiceProvider?.GetService<IHttpClientFactory>();

            if (_SingleInstance)
            {
                if (_HttpClient == null)
                {
                    lock (_lock)
                    {
                        if (_HttpClient == null)
                        {
                            _HttpClient = CreateHttpClient();
                        }
                    }
                }

                return _HttpClient;
            }
            else
            {
                return CreateHttpClient();
            }

#else
            if (_HttpClient == null)
            {
                lock (_lock)
                {
                    if (_HttpClient == null)
                    {
                        _HttpClient = CreateHttpClient();
                    }
                }
            }

            return _HttpClient;
#endif
        }

        #endregion
    }
}