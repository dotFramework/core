﻿using System;
using System.Collections.Generic;

namespace DotFramework.Core.Web
{
    public abstract class HttpRequestObject
    {
        public string Path { get; set; }
        public AuthorizationToken Token { get; set; }
        public IDictionary<String, String> Headers { get; set; }
    }

    public class HttpRequestQueryObject : HttpRequestObject
    {
        public IEnumerable<KeyValuePair<String, String>> Parameters { get; set; }
    }

    public class HttpRequestBodyObject : HttpRequestObject
    {
        public object PayLoad { get; set; }
    }
}
