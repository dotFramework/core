﻿using DotFramework.Core.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DotFramework.Core.Web
{
    public partial class WebRequestUtility : AbstractHttpUtility
    {
        #region Constructor

        public WebRequestUtility(string baseAddress) : base(baseAddress)
        {
        }

        public WebRequestUtility(string baseAddress, IDictionary<String, String> defaultHeaders) : base(baseAddress, defaultHeaders)
        {

        }

        #endregion

        #region Sync

        #region Post

        public override object Post<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            string contentType;
            string content;

            if (requestObject.PayLoad is IEnumerable<KeyValuePair<String, String>>)
            {
                contentType = "application/x-www-form-urlencoded";
                content = GetUrlEncodedRequestContent(requestObject.PayLoad as IEnumerable<KeyValuePair<String, String>>);
            }
            else
            {
                contentType = ContentType;
                content = GetJsonRequestContent(requestObject.PayLoad);
            }

            return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Post, contentType, content, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Get

        public override object Get<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Get, null, null, requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Post

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            string contentType;
            string content;

            if (requestObject.PayLoad is IEnumerable<KeyValuePair<String, String>>)
            {
                contentType = "application/x-www-form-urlencoded";
                content = GetUrlEncodedRequestContent(requestObject.PayLoad as IEnumerable<KeyValuePair<String, String>>);
            }
            else
            {
                contentType = ContentType;
                content = GetJsonRequestContent(requestObject.PayLoad);
            }

            return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Delete, contentType, content, requestObject.Token, requestObject.Headers);
        }

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Delete, null, null, requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override object Put<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            string contentType;
            string content;

            if (requestObject.PayLoad is IEnumerable<KeyValuePair<String, String>>)
            {
                contentType = "application/x-www-form-urlencoded";
                content = GetUrlEncodedRequestContent(requestObject.PayLoad as IEnumerable<KeyValuePair<String, String>>);
            }
            else
            {
                contentType = ContentType;
                content = GetJsonRequestContent(requestObject.PayLoad);
            }

            return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Put, contentType, content, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #endregion

        #region Async

        public override async Task<object> PostAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            return await Task.Run(() =>
            {
                return Post<TErrorResult>(responseModelType, requestObject);
            });
        }

        public override async Task<object> GetAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await Task.Run(() =>
            {
                return Get<TErrorResult>(responseModelType, requestObject);
            });
        }

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            return await Task.Run(() =>
            {
                return Delete<TErrorResult>(responseModelType, requestObject);
            });
        }

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await Task.Run(() =>
            {
                return Delete<TErrorResult>(responseModelType, requestObject);
            });
        }

        public override async Task<object> PutAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            return await Task.Run(() =>
            {
                return Put<TErrorResult>(responseModelType, requestObject);
            });
        }

        #endregion

        #region Helpers

        private object Send<TErrorResult>(Type responseModelType, string path, HttpMethod method, string contentType, string content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            try
            {
                return PureSend<TErrorResult>(responseModelType, path, method, contentType, content, token, headers);
            }
            catch (WebException ex)
            {
                if ((ex.Response as HttpWebResponse) != null)
                {
                    throw HandleException<TErrorResult>((ex.Response as HttpWebResponse).StatusCode, GetResponseContent(ex.Response));
                }
                else
                {
                    throw HandleException<TErrorResult>(HttpStatusCode.NotFound, ex.Message);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private object PureSend<TErrorResult>(Type responseModelType, string path, HttpMethod method, string contentType, string content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            try
            {
                HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(new Uri(BaseAddress, path));
                WriteHeaders(webRequest, method, contentType, token, headers);
                WriteContent(webRequest, content);

                using (var response = webRequest.GetResponse())
                {
                    string responseStr = GetResponseContent(response);

                    if (responseModelType == typeof(String))
                    {
                        return responseStr;
                    }
                    else
                    {
                        return JsonSerializerHelper.Deserialize(responseModelType, responseStr);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void WriteHeaders(HttpWebRequest webRequest, HttpMethod method, string contentType, AuthorizationToken token, IDictionary<String, String> headers)
        {
            try
            {
                webRequest.Accept = Accept;

                if (!String.IsNullOrEmpty(contentType))
                {
                    webRequest.ContentType = contentType;
                }

                webRequest.Method = method.Method;

                if (Timeout != 0)
                {
                    webRequest.Timeout = Timeout;
                }

                if (token != null)
                {
                    webRequest.PreAuthenticate = true;
                    webRequest.Headers.Add("Authorization", String.Format("{0} {1}", token.TokenType, token.Token));
                }

                if (DefaultHeaders != null)
                {
                    foreach (var headerItem in DefaultHeaders)
                    {
                        webRequest.Headers.Add(headerItem.Key, headerItem.Value);
                    }
                }

                if (headers != null)
                {
                    foreach (var headerItem in headers)
                    {
                        webRequest.Headers.Add(headerItem.Key, headerItem.Value);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void WriteContent(HttpWebRequest webRequest, string content)
        {
            try
            {
                if (!String.IsNullOrEmpty(content))
                {
                    ASCIIEncoding encoding = new ASCIIEncoding();
                    Byte[] bytes = encoding.GetBytes(content);

                    Stream newStream = webRequest.GetRequestStream();
                    newStream.Write(bytes, 0, bytes.Length);
                    newStream.Close();
                }
                else
                {
                    webRequest.ContentLength = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetUrlEncodedRequestContent(IEnumerable<KeyValuePair<String, String>> parameters)
        {
            return CreateQueryString(parameters);
        }

        private string GetJsonRequestContent(object model)
        {
            try
            {
                if (model != null)
                {
                    return JsonSerializerHelper.SimpleSerialize(model);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private string GetResponseContent(WebResponse response)
        {
            try
            {
                using (var stream = response.GetResponseStream())
                using (var reader = new StreamReader(stream))
                {
                    var content = reader.ReadToEnd();
                    return FixJson(content);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
