﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace DotFramework.Core.Web
{
    public class ProblemDetailsErrorResult : ErrorResult
    {
        [JsonProperty("ErrorCode")]
        public string Detail { get; set; }

        [JsonProperty("Extensions")]
        public IDictionary<string, object> Extensions { get; set; }

        [JsonProperty("Instance")]
        public string Instance { get; set; }

        [JsonProperty("Status")]
        public int? Status { get; set; }

        [JsonProperty("Title")]
        public string Title { get; set; }

        [JsonProperty("Type")]
        public string Type { get; set; }
    }
}
