﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DotFramework.Core.Web
{
    public class EnhancedHttpClientUtility : AbstractHttpClientUtility
    {
        #region Variables

        

        #endregion

        #region Constructor

        public EnhancedHttpClientUtility(string httpClientName) : this(httpClientName, false)
        {
            
        }

        public EnhancedHttpClientUtility(string httpClientName, bool singleInstance) : base(httpClientName, singleInstance)
        {

        }

        #endregion

        protected override HttpClient CreateHttpClient()
        {
            if (_HttpClientFactory == null)
            {
                throw new ArgumentNullException(nameof(_HttpClientFactory));
            }

            return _HttpClientFactory.CreateClient(_HttpClientName);
        }
    }
}