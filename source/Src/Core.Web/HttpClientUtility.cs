﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DotFramework.Core.Web
{
    public class HttpClientUtility : AbstractHttpClientUtility
    {
        #region Variables

        private readonly int _MaxConnectionsPerServer;

        #endregion

        #region Constructor

        public HttpClientUtility(string baseAddress) : this(baseAddress, null, 0)
        {
        }

        public HttpClientUtility(string baseAddress, int maxConnectionsPerServer) : this(baseAddress, null, maxConnectionsPerServer)
        {
        }

        public HttpClientUtility(string baseAddress, IDictionary<String, String> defaultHeaders) : this(baseAddress, defaultHeaders, 0)
        {

        }

        public HttpClientUtility(string baseAddress, IDictionary<String, String> defaultHeaders, int maxConnectionsPerServer) : base(baseAddress, defaultHeaders)
        {
            _MaxConnectionsPerServer = maxConnectionsPerServer;
        }

        #endregion

        #region Helpers

        private void SetHttpClientHeaders(HttpClient client)
        {
            client.BaseAddress = BaseAddress;
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Accept));

            if (DefaultHeaders != null)
            {
                foreach (var headerItem in DefaultHeaders)
                {
                    client.DefaultRequestHeaders.Add(headerItem.Key, headerItem.Value);
                }
            }

            if (Timeout != 0)
            {
                client.Timeout = TimeSpan.FromMilliseconds(Timeout);
            }
        }

        #endregion

#if NETCOREAPP3_1
        private HttpClientHandler CreateHttpClientHandler()
        {
            var handler = new HttpClientHandler
            {
                ServerCertificateCustomValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true,
            };

            if (_MaxConnectionsPerServer != 0)
            {
                handler.MaxConnectionsPerServer = _MaxConnectionsPerServer;
            }

            //if (handler.SupportsAutomaticDecompression)
            //{
            //    handler.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            //}

            return handler;
        }

        protected override HttpClient CreateHttpClient()
        {
            var handler = CreateHttpClientHandler();

            HttpClient client;

            if (_HttpClientFactory == null)
            {
                client = new HttpClient(handler, false);
            }
            else
            {
                client = _HttpClientFactory.CreateClient();
            }

            SetHttpClientHeaders(client);

            return client;
        }
#else
        protected override HttpClient CreateHttpClient()
        {
            var client = new HttpClient();
            SetHttpClientHeaders(client);

            return client;
        }
#endif
    }
}