﻿using DotFramework.Core.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Reflection;

namespace DotFramework.Core
{
    public class HttpException : ExceptionBase, IHttpException
    {
        #region Properties

        public override string RFC => "https://dotframework.net/rfc10003/http-exception";

        public override string Title => "Http Exception";

        public string ErrorContent { get; private set; }

        #endregion

        #region Constructors

        public HttpException()
        {
        }

        public HttpException(string message) : base(message)
        {
        }

        public HttpException(string message, string errorContent) : base(message)
        {
            ErrorContent = errorContent;
        }

        public HttpException(string message, Exception inner) : base(message, inner)
        {
        }

        public HttpException(string message, string errorContent, Exception inner) : base(message, inner)
        {
            ErrorContent = errorContent;
        }

        public HttpException(string message, string applicationCode, MethodBase methodBase) : base(message, applicationCode, methodBase)
        {
        }

        public HttpException(string message, string errorContent, string applicationCode, MethodBase methodBase) : base(message, applicationCode, methodBase)
        {
            ErrorContent = errorContent;
        }

        public HttpException(string message, string applicationCode, string className, string methodName) : base(message, applicationCode, className, methodName)
        {
        }

        public HttpException(string message, string errorContent, string applicationCode, string className, string methodName) : base(message, applicationCode, className, methodName)
        {
            ErrorContent = errorContent;
        }

        public HttpException(string message, Exception inner, string applicationCode, MethodBase methodBase) : base(message, inner, applicationCode, methodBase)
        {
        }

        public HttpException(string message, string errorContent, Exception inner, string applicationCode, MethodBase methodBase) : base(message, inner, applicationCode, methodBase)
        {
            ErrorContent = errorContent;
        }

        public HttpException(string message, Exception inner, string applicationCode, string className, string methodName) : base(message, inner, applicationCode, className, methodName)
        {
        }

        public HttpException(string message, string errorContent, Exception inner, string applicationCode, string className, string methodName) : base(message, inner, applicationCode, className, methodName)
        {
            ErrorContent = errorContent;
        }

        #endregion
    }

    public class HttpException<T> : HttpException
    {
        #region Constructor

        public HttpException(string message, string errorContent)
            : base(message, errorContent)
        {
            ErrorResult = JsonConvert.DeserializeObject<T>(errorContent);
        }

        public HttpException(string message, JObject obj)
            : base(message)
        {
            ErrorResult = obj.ToObject<T>();
        }

        #endregion

        #region Properties

        public T ErrorResult { get; private set; }

        #endregion
    }

    public class ProblemDetailsHttpException : HttpException<ProblemDetailsErrorResult>
    {
        public ProblemDetailsHttpException(string message, string errorContent) : base(message, errorContent)
        {
            SetOriginalTraceID();
        }

        public ProblemDetailsHttpException(string message, JObject obj) : base(message, obj)
        {
            SetOriginalTraceID();
        }

        private void SetOriginalTraceID()
        {
            Guid.TryParse(ErrorResult.Extensions["traceId"]?.ToString(), out var originalTraceID);
            OriginalTraceID = originalTraceID;
        }
    }

    public class ProblemDetailsCustomHttpException : ProblemDetailsHttpException, ICustomException
    {
        public ProblemDetailsCustomHttpException(string message, string errorContent) : base(message, errorContent)
        {
        }

        public ProblemDetailsCustomHttpException(string message, JObject obj) : base(message, obj)
        {
        }
    }

    public class UnauthorizedHttpException : HttpException, IUnauthorizedHttpException
    {
        public override string RFC => "https://dotframework.net/rfc10004/unauthorized-http-exception";

        public override string Title => "Unauthorized Http Exception";

        public override int ErrorCode { get; set; } = 401;

        public UnauthorizedHttpException()
        {
        }

        public UnauthorizedHttpException(string message) : base(message)
        {
        }

        public UnauthorizedHttpException(string message, Exception inner) : base(message, inner)
        {
        }
    }

    public class UnauthorizedHttpException<T> : UnauthorizedHttpException
    {
        #region Constructor

        public UnauthorizedHttpException(string errorContent)
            : base()
        {
            ErrorResult = JsonConvert.DeserializeObject<T>(errorContent);
        }

        public UnauthorizedHttpException(string message, string errorContent)
            : base(message)
        {
            ErrorResult = JsonConvert.DeserializeObject<T>(errorContent);
        }

        public UnauthorizedHttpException(JObject obj)
            : base()
        {
            ErrorResult = obj.ToObject<T>();
        }

        public UnauthorizedHttpException(string message, JObject obj)
            : base(message)
        {
            ErrorResult = obj.ToObject<T>();
        }

        #endregion

        #region Properties

        public T ErrorResult { get; private set; }

        #endregion
    }
}
