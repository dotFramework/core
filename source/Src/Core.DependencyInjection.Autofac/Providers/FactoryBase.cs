﻿using Autofac;
using Autofac.Builder;
using Autofac.Core;
using Autofac.Extras.DynamicProxy;
using Autofac.Features.Scanning;
using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DotFramework.Core.DependencyInjection
{
    public abstract class FactoryBase<TFactory, TBaseType> : SingletonProvider<TFactory>
        where TFactory : class
    {
        private readonly ContainerBuilder _Builder;
        private readonly List<Type> _Intercepors;

        protected virtual InterceptionMethod InterceptionMethod => InterceptionMethod.None;

        protected FactoryBase()
        {
            _Builder = new ContainerBuilder();
            _Intercepors = new List<Type>();
        }

        private IContainer _Container;
        protected IContainer Container
        {
            get
            {
                if (_Container != null)
                {
                    return _Container;
                }
                else
                {
                    throw new InvalidOperationException("Container is not ready yet.");
                }
            }
            private set
            {
                _Container = value;
            }
        }

        protected void AddInterceptor<TInterceptor>() where TInterceptor : IInterceptor
        {
            if (InterceptionMethod != InterceptionMethod.None)
            {
                _Builder.RegisterType<TInterceptor>();
                _Intercepors.Add(typeof(TInterceptor));
            }
            else
            {
                throw new InvalidOperationException("Interception Method has not been set.");
            }
        }

        protected void Build()
        {
            _Container = _Builder.Build();
        }

        public virtual TType Resolve<TType>() where TType : TBaseType
        {
            try
            {
                return Container.Resolve<TType>();
            }
            catch (Exception ex)
            {
                if (HandleException(ref ex))
                {
                    throw ex;
                }

                return default(TType);
            }
        }

        protected virtual void RegisterType<TImplementer>() where TImplementer : class, TBaseType
        {
            RegisterType(typeof(TImplementer));
        }

        protected virtual void RegisterType(Type implementerType)
        {
            var builder =
                _Builder
                    .RegisterType(implementerType)
                    .SingleInstance();

            //Only Class
            if (_Intercepors.Any())
            {
                if (InterceptionMethod != InterceptionMethod.Class)
                {
                    throw new InvalidOperationException("Invalid Interception Method");
                }

                builder = builder.EnableClassInterceptors();

                foreach (var intercepor in _Intercepors)
                {
                    builder = builder.InterceptedBy(intercepor);
                }
            }
        }

        protected virtual void RegisterType<TImplementer, TService>() where TImplementer : TBaseType, TService
        {
            RegisterType(typeof(TImplementer), typeof(TService));
        }

        protected virtual void RegisterType(Type implementerType, Type serviceType)
        {
            var builder =
                _Builder
                    .RegisterType(implementerType)
                    .As(serviceType)
                    .SingleInstance();

            //Both Class And Interface Based On TBaseType
            if (_Intercepors.Any())
            {
                switch (InterceptionMethod)
                {
                    case InterceptionMethod.Interface:
                        builder = builder.EnableInterfaceInterceptors();
                        break;
                    case InterceptionMethod.Class:
                        builder.EnableClassInterceptors();
                        break;
                    default:
                        throw new InvalidOperationException("Invalid Interception Method");
                }

                foreach (var intercepor in _Intercepors)
                {
                    builder = builder.InterceptedBy(intercepor);
                }
            }
        }

        //protected virtual void Register<TImplementer>(TImplementer instance) where TImplementer : class, TBaseType
        //{
        //    Register(instance);
        //}

        //protected virtual void Register(object instance)
        //{
        //    var builder =
        //        _Builder
        //            .RegisterInstance(instance);

        //    //Only Interface
        //    if (_Intercepors.Any())
        //    {
        //        if (InterceptionMethod != InterceptionMethod.Interface)
        //        {
        //            throw new InvalidOperationException("Invalid Interception Method");
        //        }

        //        builder = builder.EnableInterfaceInterceptors();

        //        foreach (var intercepor in _Intercepors)
        //        {
        //            builder = builder.InterceptedBy(intercepor);
        //        }
        //    }
        //}

        protected virtual void Register<TImplementer, TService>(TImplementer instance) where TImplementer : class, TBaseType, TService
        {
            Register(instance, typeof(TService));
        }

        protected virtual void Register(object instance, Type serviceType)
        {
            var builder =
                _Builder
                    .RegisterInstance(instance)
                    .As(serviceType);

            //Only Interface
            if (_Intercepors.Any())
            {
                if (InterceptionMethod != InterceptionMethod.Interface)
                {
                    throw new InvalidOperationException("Invalid Interception Method");
                }

                builder = builder.EnableInterfaceInterceptors();

                foreach (var intercepor in _Intercepors)
                {
                    builder = builder.InterceptedBy(intercepor);
                }
            }
        }

        protected virtual void Register(params Assembly[] assemblies)
        {
            var builder =
                _Builder
                    .RegisterAssemblyTypes(assemblies)
                    .Where(x => x.IsAssignableTo<TBaseType>());

            //Both Class And Interface Based On TBaseType
            if (_Intercepors.Any())
            {
                if (InterceptionMethod != InterceptionMethod.Class)
                {
                    throw new InvalidOperationException("Invalid Interception Method");
                }

                builder = builder.EnableClassInterceptors();

                foreach (var intercepor in _Intercepors)
                {
                    builder = builder.InterceptedBy(intercepor);
                }
            }
        }

        protected abstract bool HandleException(ref Exception ex);
    }

    public abstract class FactoryBase<TFactory, TBaseType, TInterceptor> : FactoryBase<TFactory, TBaseType>
    where TFactory : class
    where TInterceptor : IInterceptor
    {
        protected FactoryBase()
        {
            AddInterceptor<TInterceptor>();
        }
    }
}
