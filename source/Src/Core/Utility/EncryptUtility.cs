using System;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace DotFramework.Core
{
    public static class EncryptUtility
    {
        public static string EncryptText(string text)
        {
            Byte[] byteArray = ASCIIEncoding.ASCII.GetBytes(text);
            string encryptedConnectionString = Convert.ToBase64String(byteArray);

            return encryptedConnectionString;
        }

        public static string DecryptText(string text)
        {
            Byte[] byteArray = Convert.FromBase64String(text);
            string decryptedConnectionString = ASCIIEncoding.ASCII.GetString(byteArray);

            return decryptedConnectionString;
        }

        public static string GetHash(string text, string key)
        {
            using (HMACSHA256 hMACSHA256 = new HMACSHA256(Encoding.ASCII.GetBytes(key)))
            {
                byte[] numArray = hMACSHA256.ComputeHash(Encoding.ASCII.GetBytes(text));
                StringBuilder stringBuilder = new StringBuilder();

                for (int i = 0; i < (int)numArray.Length; i++)
                {
                    stringBuilder.Append(numArray[i].ToString("x2", CultureInfo.InvariantCulture));
                }

                return stringBuilder.ToString();
            }
        }
    }
}
