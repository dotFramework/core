﻿using System;

namespace DotFramework.Core
{
    public class ServiceContext : SingletonProvider<ServiceContext>
    {
        private ServiceContext()
        {

        }

        public IServiceProvider ServiceProvider { get; private set; }

        public void Initialize(IServiceProvider serviceProvider)
        {
            ServiceProvider = serviceProvider;
        }
    }
}
