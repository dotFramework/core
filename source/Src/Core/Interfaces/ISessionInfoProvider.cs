﻿namespace DotFramework.Core
{
    public interface ISessionInfoProvider
    {
        string GetConnectionID();
        string GetSessionID();
        string GetContextID();
        string GetUserName();
        string GetHost();
        string GetUrl();
        string GetReferer();
        string GetTenant();
    }
}
