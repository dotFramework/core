﻿using System;
using System.Linq;
using System.Reflection;

namespace DotFramework.Core
{
    public abstract class ExceptionBase : Exception, IExceptionBase
    {
        public abstract string RFC { get; }
        public abstract string Title { get; }
        public virtual int ErrorCode { get; set; } = 400;

        private readonly string[] _dataProperties = new string[] { "Trace ID", "Original Trace ID", "Application Code", "Class Name", "Method Name", "UserName", "ConnectionID", "SessionID", "ContextID", "Host", "Url", "Referer", "Tenant" };

        #region Constructors

        public ExceptionBase()
            : base()
        {
            Init();
        }

        public ExceptionBase(string message)
            : base(message)
        {
            Init();
        }

        public ExceptionBase(string message, string applicationCode, string className, string methodName)
            : base(message)
        {
            Init();

            ApplicationCode = applicationCode;
            ClassName = className;
            MethodName = methodName;
        }

        public ExceptionBase(string message, string applicationCode, MethodBase methodBase) :
            this(message, applicationCode, methodBase.DeclaringType.FullName, methodBase.Name)
        {

        }

        public ExceptionBase(string message, Exception inner)
            : base(message, inner)
        {
            if (inner is ExceptionBase)
            {
                Copy(inner as ExceptionBase);
            }
            else
            {
                Init(inner);
            }
        }

        public ExceptionBase(string message, Exception inner, string applicationCode, string className, string methodName)
            : base(message, inner)
        {
            Init(inner);

            ApplicationCode = applicationCode;
            ClassName = className;
            MethodName = methodName;
        }

        public ExceptionBase(string message, Exception inner, string applicationCode, MethodBase methodBase) :
            this(message, inner, applicationCode, methodBase.DeclaringType.FullName, methodBase.Name)
        {

        }

        #endregion

        #region Properties

        private Guid _TraceID;
        public Guid TraceID
        {
            get
            {
                return _TraceID;
            }
            private set
            {
                _TraceID = value;
                ModifyData("Trace ID", value.ToString());
            }
        }

        private Guid? _OriginalTraceID;
        public Guid? OriginalTraceID
        {
            get
            {
                return _OriginalTraceID;
            }
            protected set
            {
                _OriginalTraceID = value;
                ModifyData("Original Trace ID", value.ToString());
            }
        }

        private string _ApplicationCode;
        public string ApplicationCode
        {
            get
            {
                return _ApplicationCode;
            }
            set
            {
                _ApplicationCode = value;
                ModifyData("Application Code", value);
            }
        }

        private string _ClassName;
        public string ClassName
        {
            get
            {
                return _ClassName;
            }
            set
            {
                _ClassName = value;
                ModifyData("Class Name", value);
            }
        }

        private string _MethodName;
        public string MethodName
        {
            get
            {
                return _MethodName;
            }
            set
            {
                _MethodName = value;
                ModifyData("Method Name", value);
            }
        }

        private string _UserName;
        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
                ModifyData("UserName", value);
            }
        }

        private string _ConnectionID;
        public string ConnectionID
        {
            get
            {
                return _ConnectionID;
            }
            set
            {
                _ConnectionID = value;
                ModifyData("ConnectionID", value);
            }
        }

        private string _SessionID;
        public string SessionID
        {
            get
            {
                return _SessionID;
            }
            set
            {
                _SessionID = value;
                ModifyData("SessionID", value);
            }
        }

        private string _ContextID;
        public string ContextID
        {
            get
            {
                return _ContextID;
            }
            set
            {
                _ContextID = value;
                ModifyData("ContextID", value);
            }
        }

        private string _Host;
        public string Host
        {
            get
            {
                return _Host;
            }
            set
            {
                _Host = value;
                ModifyData("Host", value);
            }
        }

        private string _Url;
        public string Url
        {
            get
            {
                return _Url;
            }
            set
            {
                _Url = value;
                ModifyData("Url", value);
            }
        }

        private string _Referer;
        public string Referer
        {
            get
            {
                return _Referer;
            }
            set
            {
                _Referer = value;
                ModifyData("Referer", value);
            }
        }

        private string _Tenant;
        public string Tenant
        {
            get
            {
                return _Tenant;
            }
            set
            {
                _Tenant = value;
                ModifyData("Tenant", value);
            }
        }

        #endregion

        #region Private Methods

        protected void ModifyData(object key, object value)
        {
            if (value != null)
            {
                if (!Data.Contains(key))
                {
                    Data.Add(key, value);
                }
                else
                {
                    Data[key] = value;
                }
            }
            else
            {
                Data.Remove(key);
            }
        }

        protected void Init(Exception ex)
        {
            CopyData(ex);
            Init();
        }

        protected void Init()
        {
            TraceID = Guid.NewGuid();

            ISessionInfoProvider sessionInfoProvider = ServiceContext.Instance?.ServiceProvider?.GetService(typeof(ISessionInfoProvider)) as ISessionInfoProvider;

            if (sessionInfoProvider != null)
            {
                UserName = sessionInfoProvider?.GetUserName();
                ConnectionID = sessionInfoProvider?.GetConnectionID();
                SessionID = sessionInfoProvider?.GetSessionID();
                ContextID = sessionInfoProvider?.GetContextID();
                Host = sessionInfoProvider?.GetHost();
                Url = sessionInfoProvider?.GetUrl();
                Referer = sessionInfoProvider?.GetReferer();
                Tenant = sessionInfoProvider?.GetTenant();
            }
        }

        protected void Copy(ExceptionBase exception)
        {
            CopyData(exception);

            ErrorCode = exception.ErrorCode;
            TraceID = exception.TraceID;
            OriginalTraceID = exception.OriginalTraceID;
            ApplicationCode = exception.ApplicationCode;
            ClassName = exception.ClassName;
            MethodName = exception.MethodName;
            UserName = exception.UserName;
            ConnectionID = exception.ConnectionID;
            SessionID = exception.SessionID;
            ContextID = exception.ContextID;
            Host = exception.Host;
            Url = exception.Url;
            Referer = exception.Referer;
            Tenant = exception.Tenant;
        }

        private void CopyData(Exception ex)
        {
            if (ex?.Data?.Count != 0)
            {
                foreach (var key in ex.Data.Keys)
                {
                    if (!_dataProperties.Contains(key))
                    {
                        ModifyData(key, ex.Data[key]);
                    }
                }
            }
        }

        #endregion
    }
}
