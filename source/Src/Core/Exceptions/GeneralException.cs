﻿using System;
using System.Reflection;

namespace DotFramework.Core
{
    public class GeneralException : ExceptionBase
    {
        public override string RFC => "https://dotframework.net/rfc10001/general-exception";

        public override string Title => "General Exception";

        public GeneralException()
        {
        }

        public GeneralException(string message) : base(message)
        {
        }

        public GeneralException(string message, Exception inner) : base(message, inner)
        {
        }

        public GeneralException(string message, string applicationCode, MethodBase methodBase) : base(message, applicationCode, methodBase)
        {
        }

        public GeneralException(string message, string applicationCode, string className, string methodName) : base(message, applicationCode, className, methodName)
        {
        }

        public GeneralException(string message, Exception inner, string applicationCode, MethodBase methodBase) : base(message, inner, applicationCode, methodBase)
        {
        }

        public GeneralException(string message, Exception inner, string applicationCode, string className, string methodName) : base(message, inner, applicationCode, className, methodName)
        {
        }
    }
}
