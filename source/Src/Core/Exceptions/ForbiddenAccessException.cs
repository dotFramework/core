﻿using System;

namespace DotFramework.Core
{
    public class ForbiddenAccessException : ExceptionBase
    {
        public override string RFC => "https://dotframework.net/rfc10004/forbidden-access-exception";

        public override string Title => "Forbidden Access Exception";

        public ForbiddenAccessException() : base("Access Denied")
        {
        }

        public ForbiddenAccessException(string message) : base(message)
        {
        }
    }
}
