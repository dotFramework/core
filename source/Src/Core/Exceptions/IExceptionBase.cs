﻿using System;

namespace DotFramework.Core
{
    public interface IExceptionMetadata
    {
        string RFC { get; }
        string Title { get; }
    }

    public interface IExceptionBase : IExceptionMetadata
    {
        Guid TraceID { get; }
        string ApplicationCode { get; set; }
        string ClassName { get; set; }
        string MethodName { get; set; }
    }
}
