﻿namespace System
{
    public static class DateTimeExtensions
    {
        public static DateTime ConvertTime(this DateTime dateTime, string timeZone)
        {
            if (String.IsNullOrEmpty(timeZone))
            {
                throw new ArgumentNullException("TimeZone");
            }

            if (dateTime.Kind != DateTimeKind.Utc)
            {
                throw new InvalidTimeZoneException();
            }

            return TimeZoneInfo.ConvertTimeFromUtc(dateTime, TimeZoneInfo.FindSystemTimeZoneById(timeZone));
        }

        public static DateTimeOffset ConvertTime(this DateTimeOffset dateTimeOffset, string timeZone)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(dateTimeOffset, timeZone);
        }

        public static DateTime ConvertToUtc(this DateTime dateTime, string timeZoneId)
        {
            try
            {
                // Find the time zone by its ID
                TimeZoneInfo timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);

                // Convert the provided dateTime to UTC using the time zone
                DateTime utcDateTime = TimeZoneInfo.ConvertTimeToUtc(dateTime, timeZone);

                return utcDateTime;
            }
            catch (TimeZoneNotFoundException)
            {
                throw new ArgumentException($"The time zone ID '{timeZoneId}' was not found.");
            }
            catch (InvalidTimeZoneException)
            {
                throw new ArgumentException($"The time zone ID '{timeZoneId}' is invalid.");
            }
        }
    }
}
