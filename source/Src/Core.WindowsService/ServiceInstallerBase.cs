﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Configuration.Install;
using System.ServiceProcess;

namespace DotFramework.Core.WindowsService
{
    public class ServiceInstallerBase : Installer
    {
        #region Private Members

        protected readonly string ServiceName;

        protected readonly string DisplayName;

        protected readonly string Description;

        #endregion

        #region Constructor

        public ServiceInstallerBase()
        {
            try
            {
                var config = ConfigurationManager.OpenExeConfiguration(this.GetType().Assembly.Location);

                ServiceName = config.AppSettings.Settings["ServiceName"].Value;
                DisplayName = config.AppSettings.Settings["DisplayName"].Value;
                Description = config.AppSettings.Settings["Description"].Value;

                ServiceProcessInstaller process = new ServiceProcessInstaller();

                process.Account = Account;

                ServiceInstaller serviceAdmin = new ServiceInstaller();

                serviceAdmin.BeforeInstall += OnBeforeInstall;
                serviceAdmin.AfterInstall += OnAfterInstall;
                serviceAdmin.BeforeUninstall += OnBeforeUninstall;
                serviceAdmin.AfterUninstall += OnAfterUninstall;

                serviceAdmin.StartType = StartMode;

                serviceAdmin.ServiceName = ServiceName;
                serviceAdmin.DisplayName = DisplayName;
                serviceAdmin.Description = Description;

                Installers.Add(process);
                Installers.Add(serviceAdmin);
            }
            catch (ArgumentNullException ex)
            {
                throw ex;
            }
            catch (ArgumentException ex)
            {
                throw ex;
            }
            catch (Win32Exception ex)
            {
                throw ex;
            }
            catch (InvalidOperationException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region Protected Members

        protected virtual ServiceAccount Account
        {
            get
            {
                return ServiceAccount.LocalSystem;
            }
        }

        protected virtual ServiceStartMode StartMode
        {
            get
            {
                return ServiceStartMode.Automatic;
            }
        }

        #endregion

        #region Private Events

        public void OnBeforeInstall(Object sender, InstallEventArgs e)
        {
            return;
        }

        public void OnAfterInstall(Object sender, InstallEventArgs e)
        {
            try
            {
                if (StartMode == ServiceStartMode.Automatic)
                {
                    foreach (ServiceController serviceControl in ServiceController.GetServices())
                    {
                        if (String.Compare(serviceControl.ServiceName, ServiceName) == 0)
                        {
                            if (serviceControl.Status != ServiceControllerStatus.Running)
                                serviceControl.Start();

                            break;
                        }
                    }
                }
            }
            catch (ArgumentNullException)
            {
            }
            catch (ArgumentException)
            {
            }
            catch (Win32Exception)
            {
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception)
            {
            }

            return;
        }

        public void OnBeforeUninstall(Object sender, InstallEventArgs e)
        {
            try
            {
                foreach (ServiceController serviceControl in ServiceController.GetServices())
                {
                    if (String.Compare(serviceControl.ServiceName, ServiceName) == 0)
                    {
                        if (serviceControl.Status != ServiceControllerStatus.Stopped)
                        {
                            serviceControl.Stop();
                        }

                        break;
                    }
                }
            }
            catch (ArgumentNullException)
            {
            }
            catch (ArgumentException)
            {
            }
            catch (Win32Exception)
            {
            }
            catch (InvalidOperationException)
            {
            }
            catch (Exception)
            {
            }

            return;
        }

        public void OnAfterUninstall(Object sender, InstallEventArgs e)
        {
            return;
        }

        #endregion
    }
}
