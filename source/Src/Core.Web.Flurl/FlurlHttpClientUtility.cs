﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Flurl;
using Flurl.Http;
using System.Net;
using System.Text.RegularExpressions;
using System.Linq;

namespace DotFramework.Core.Web
{
    public class FlurlHttpClientUtility : AbstractHttpUtility
    {
        #region Variables

        protected readonly string _HttpClientName;

        #endregion

        #region Constructor

        public FlurlHttpClientUtility(string baseAddress) : base(baseAddress)
        {

        }

        #endregion

        #region Sync

        #region Post

        public override object Post<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Post, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get

        public override object Get<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Get, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Delete

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Delete, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override object Delete<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return Send<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Delete, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Put

        public override object Put<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return Send<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Put, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion

        #region Async

        #region Post

        public override async Task<object> PostAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Post, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get

        public override async Task<object> GetAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await SendAsync<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Get, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Delete

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Delete, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override async Task<object> DeleteAsync<TErrorResult>(Type responseModelType, HttpRequestQueryObject requestObject)
        {
            return await SendAsync<TErrorResult>(responseModelType, AppendQueryString(requestObject.Path, requestObject.Parameters), HttpMethod.Delete, null, requestObject.Token, requestObject.Headers);
        }

        #endregion

        #region Put

        public override async Task<object> PutAsync<TErrorResult>(Type responseModelType, HttpRequestBodyObject requestObject)
        {
            try
            {
                return await SendAsync<TErrorResult>(responseModelType, requestObject.Path, HttpMethod.Put, GetContent(requestObject.PayLoad), requestObject.Token, requestObject.Headers);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #endregion

        #region Helpers

        private HttpContent GetContent(object model)
        {
            HttpContent content;

            if (model is HttpContent)
            {
                content = model as HttpContent;
            }
            else if (model is IEnumerable<KeyValuePair<String, String>>)
            {
                content = GetUrlEncodedRequestContent(model as IEnumerable<KeyValuePair<String, String>>);
            }
            else
            {
                content = GetStringContent(model);
            }

            return content;
        }

        private StringContent GetStringContent(object model)
        {
            try
            {
                StringContent content = null;

                if (model != null)
                {
                    content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, ContentType);
                }

                return content;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private FormUrlEncodedContent GetUrlEncodedRequestContent(IEnumerable<KeyValuePair<String, String>> parameters)
        {
            try
            {
                return new FormUrlEncodedContent(parameters);
            }
            catch (Exception)
            {
                throw;
            }
        }

        private object Send<TErrorResult>(Type responseModelType, string path, HttpMethod method, HttpContent content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            IFlurlRequest request = CreateRequest(BaseAddress, path, token, headers);

            try
            {
                var response = request.SendAsync(method, content).Result;
                var json = response.GetStringAsync().Result;

                if (response.IsSuccess())
                {
                    if (responseModelType == typeof(String))
                    {
                        return json;
                    }
                    else
                    {
                        return JsonConvert.DeserializeObject(json, responseModelType);
                    }
                }
                else
                {
                    throw HandleException<TErrorResult>((HttpStatusCode)response.StatusCode, json);
                }
            }
            catch (FlurlHttpException ex)
            {
                var json = ex.GetResponseStringAsync().Result;
                throw HandleException<TErrorResult>((HttpStatusCode)ex.StatusCode, json);
            }
        }

        private async Task<object> SendAsync<TErrorResult>(Type responseModelType, string path, HttpMethod method, HttpContent content, AuthorizationToken token, IDictionary<String, String> headers) where TErrorResult : ErrorResult
        {
            IFlurlRequest request = CreateRequest(BaseAddress, path, token, headers);

            try
            {
                var response = await request.SendAsync(method, content);
                var json = await response.GetStringAsync();

                if (response.IsSuccess())
                {
                    if (responseModelType == typeof(String))
                    {
                        return json;
                    }
                    else
                    {
                        return JsonConvert.DeserializeObject(json, responseModelType);
                    }
                }
                else
                {
                    throw HandleException<TErrorResult>((HttpStatusCode)response.StatusCode, json);
                }
            }
            catch (FlurlHttpException ex)
            {
                var json = await ex.GetResponseStringAsync();
                throw HandleException<TErrorResult>((HttpStatusCode)ex.StatusCode, json);
            }
        }

        private IFlurlRequest CreateRequest(Uri baseAddress, string path, AuthorizationToken token, IDictionary<string, string> headers)
        {
            var pathParts = path.Split('?');
            var absolutePath = pathParts[0];

            var url = baseAddress.AppendPathSegments(absolutePath);

            if (pathParts.Length == 2)
            {
                url.SetQueryParams(ParseQueryString(pathParts[1]));
            }

            var request = url.WithHeader("Accept", Accept);

            if (token != null)
            {
                request = request.WithHeader("Authorization", $"{token.TokenType} {token.Token}");
            }

            if (headers != null)
            {
                request = request.WithHeaders(headers);
            }

            return request;
        }

        private Dictionary<string, string> ParseQueryString(string requestQueryString)
        {
            Dictionary<string, string> rc = new Dictionary<string, string>();
            string[] ar1 = requestQueryString.Split(new char[] { '&', '?' });

            foreach (string row in ar1)
            {
                if (string.IsNullOrEmpty(row)) continue;
                int index = row.IndexOf('=');
                if (index < 0) continue;
                rc[Uri.UnescapeDataString(row.Substring(0, index))] = Uri.UnescapeDataString(row.Substring(index + 1)); // use Unescape only parts          
            }

            return rc;
        }

        #endregion
    }
}