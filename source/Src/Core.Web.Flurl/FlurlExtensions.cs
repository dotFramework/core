﻿namespace Flurl.Http
{
    internal static class FlurlExtensions
    {
        public static bool IsSuccess(this IFlurlResponse response)
        {
            return response.StatusCode >= 200 || response.StatusCode <= 299;
        }
    }
}
